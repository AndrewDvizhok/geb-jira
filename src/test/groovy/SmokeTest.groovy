import helpers.JiraUser
import org.junit.Before
import org.junit.Test

import java.time.LocalDate

class SmokeTest {

    @Before
    void prepareTest() {
        GebJira.main("-j", "http://jira:8080", "-a", "admin", "-p", "test")
    }

    @Test
    void helpTest() {
        def buffer = new ByteArrayOutputStream()
        System.out = new PrintStream(buffer)
        GebJira.main()
        assert buffer.toString().contains("usage")
    }

    @Test
    void openJira() {
        GebJira.main("-j", "http://jira:8080/", "-a", "admin", "-p", "test", "-c", "test")
        //assert GebJira.responseCode == 200 //todo response code is not simple task
        assert GebJira.lastAction == "Jira opened"
    }

    @Test
    void slashTest() {
        GebJira.main("-j", "http://jira:8080/", "-a", "admin", "-p", "test")
        assert GebJira.jiraUrl == "http://jira:8080"
    }

    @Test
    void openProfileTest() {
        GebJira.main("-j", "http://jira:8080/", "-a", "admin", "-p", "test", "-c", "profile")
        assert GebJira.lastAction == "Profile opened"
    }

    @Test
    void adminAccessTest() {
        def buffer = new ByteArrayOutputStream()
        System.out = new PrintStream(buffer)
        GebJira.main("-j", "http://jira:8080/", "-a", "admin", "-p", "test", "-c", "users")
        assert buffer.toString().contains("User list")
    }

    @Test
    void parseDataTest(){
        JiraUser u = new JiraUser()
        u.tryParseLastLoginDate("Last: 22.09.20 12:57")
        assert u.lastLogin.isEqual(LocalDate.parse("22.09.20", JiraUser.dateFormatter))
        println(u.lastLogin.toString())
        JiraUser.dateSearchRegex = "\\d{2}/.{3}/\\d{2}"
        JiraUser.dateFormatter = "dd/MMM/yy"
        u.tryParseLastLoginDate("Last: 22/Jan/19")
        assert u.lastLogin.isEqual(LocalDate.parse("22/Jan/19", JiraUser.dateFormatter))
        println(u.lastLogin.toString())
    }

}
