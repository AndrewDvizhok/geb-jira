package pages

import geb.Page

class Welcome extends Page {
    static url = "/secure/Dashboard.jspa"

    static content = {
        loginBtn { $("li", id:"user-options") }
    }
}
