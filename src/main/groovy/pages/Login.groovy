package pages

import geb.Page

class Login extends Page {
    static url = "/login.jsp"

    static content = {
        form { $("form", id: "login-form") }
        formLogin { $("input", id: "login-form-username") }
        formPass { $("input", id: "login-form-password") }
        formSend { $("input", id:"login-form-submit")}
    }
}
