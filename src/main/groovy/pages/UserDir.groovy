package pages

import geb.Page

class UserDir extends Page {
    static url = '/secure/admin/user/UserBrowser.jspa'

    static content = {
        formUserDir { $("form", action: "UserBrowser.jspa") }
        nextPage { $("a", class: "next-icon") }
        table { $("tbody") }
        rows { $("tbody>tr")}
    }
}
