package pages

import geb.Page

class Profile extends Page {
    static url = "/secure/ViewProfile.jspa"

    static content = {
        username {$("dd", id:"up-d-username")}
    }
}
