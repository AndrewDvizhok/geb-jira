import helpers.AdminCredentials
import helpers.JiraUser
import org.apache.commons.cli.CommandLine
import org.apache.commons.cli.CommandLineParser
import org.apache.commons.cli.DefaultParser
import org.apache.commons.cli.HelpFormatter
import org.apache.commons.cli.Option
import org.apache.commons.cli.Options
import org.apache.commons.cli.ParseException

@Grapes([
        @Grab(group = 'org.gebish', module = 'geb-core', version = '3.4'),
        @Grab(group = 'org.seleniumhq.selenium', module = 'selenium-support', version = '3.141.59'),
        @Grab(group = 'org.seleniumhq.selenium', module = 'selenium-chrome-driver', version = '3.141.59')
])

class GebJira {
    static Options options
    static HelpFormatter helpFormatter
    static CommandLine commands
    static String lastAction

    static void main(String... args) {

        println "Welcome to GEB for JIRA automation tool!"
        if (options == null) {
            options = defineOptions()
        }
        if (args.size() > 0) {
            Actor actor = new Actor(parseOptions(args))
            run(actor)
        } else {
            showHelp()
        }

    }

    static Options defineOptions() {

        options = new Options()
        helpFormatter = new HelpFormatter()
        options.addOption(new Option("j", "jiraHost", true, "set address of the jira instance, e.g. http://jira.example.com:8080"))
        options.addOption(new Option("a", "admin", true, "set admin username"))
        options.addOption(new Option("p", "password", true, "set admin password"))
        options.addOption(new Option("c", "command", true, "set command to run [profile]"))
        options.addOption(new Option("f", "config", true, "set config file"))
        options.addOption(new Option("sg", "src-groups", true, "source group from which users should be removed"))
        options.addOption(new Option("dg", "dst-groups", true, "destination group"))
        options.addOption(new Option("da", "days-ago", true, "Threshold - how many days ago user logged in"))
        options.addOption(new Option("il", "ignore-login", false, "Ignore login page if used sso"))
        options.addOption(new Option("dr", "date-regex", true, "Provide rexeg string for found date format"))
        options.addOption(new Option("df", "date-format", true, "Provide format for date parser"))
        options.addOption(new Option("uf", "user-filter", true, "Provide user filter"))
    }

    static Properties parseOptions(args) {

        CommandLineParser parser = new DefaultParser()
        AdminCredentials cred = new AdminCredentials()
        Properties prop = new Properties()
        try {
            commands = parser.parse(options, args)
            if(commands.hasOption("f")){
                File configFile = new File(commands.getOptionValue("f"))
                configFile.withInputStream{
                    prop.load(it)
                }
            }
        }
        catch (ParseException exp) {
            System.err.println("Parsing failed.  Reason: " + exp.getMessage())
        }
        prop

    }

    static void showHelp() {
        helpFormatter.printHelp("-j <host> -a <usernaem> -c <command>", options)
    }

    static void run(actor) {
        switch (commands.getOptionValue('c')) {
            case 'profile':
                lastAction = actor.openProfile()
                break
            case 'open':
                lastAction = actor.openJira()
                break
            case 'users':
                lastAction = actor.getUsers().size()
                break
            case 'deactivate':
                lastAction = actor.deactAllOlds()
                break
            case 'deactivateList':
                lastAction = actor.deactFromList()
                break
            case 'test':
                actor.test()
                break
        }
        if (lastAction != null) {
            println lastAction
        }
    }

}