import geb.Browser
import helpers.JiraUser

import java.time.LocalDate

class UserMover {

    Browser browser

    void deactAllOlds(String srcG, String dstG, String daysA) {
        int daysAgo
        if (daysA == null) {
            daysAgo = 180
        } else {
            daysAgo = daysA.toLong()
        }
        Set<String> srcGroups = new HashSet<>()
        Set<String> dstGroups = new HashSet<>()
        srcGroups.addAll(srcG.split(","))
        dstGroups.addAll(dstG.split(","))
        Actor actor = new Actor()
        Set<JiraUser> jiraUsers = actor.getUsers()
        browser = actor.browser
        jiraUsers.each { JiraUser ju ->
            if (ju.isNeverLogin()) {
                moveUser(ju, srcGroups, dstGroups)
            } else {
                if (ju.lastLogin.isBefore(LocalDate.now().minusDays(daysA))) {
                    moveUser(ju, srcGroups, dstGroups)
                }
            }
        }
    }

    void moveUser(user, srcG, dstG){

    }


}
