import geb.Browser
import helpers.DataWriter
import helpers.UserDirAction
import helpers.JiraUser
import pages.Profile
import pages.Welcome

class Actor implements UserDirAction {

    Browser browser

    Actor(prop) {
        browser = new Browser()
        p = prop
        browser.baseUrl = p."host".reverse().startsWith("/") ? p."host".replaceFirst("\\/\$", "") : p."host"
    }

    String openJira() {
        browser.to Welcome
        return browser.loginBtn.isDisplayed() ? 'Jira opened' : 'Can\'t open jira'
    }

    String openProfile() {
        authJira(browser)
        browser.to Profile
        return browser.username.isDisplayed() ? 'Profile opened' : 'Can\'t open profile'
    }

    Set<JiraUser> getUsers() {
        Set<JiraUser> jiraUsers = new HashSet<>()
        openUserDir(browser)
        if (browser.formUserDir.isDisplayed()) {
            while (isNextPageOnUserDir(browser)) {
                jiraUsers.addAll(getUsersFromPage(browser))
                openNextPageOnUserDir(browser)
            }
            return jiraUsers
        }
    }

    void deactFromList() {
        if (p."userList" != null) {
            File userList = new File(p."userList")
            if (userList.exists()) {
                Set<String> srcGroups = new HashSet<>()
                Set<String> dstGroups = new HashSet<>()
                srcGroups.addAll(p."srcGroups".split(","))
                dstGroups.addAll(p."dstGroups".split(","))
                userList.readLines().each { String userName ->
                    println("Move user: ${userName} - " + moveUser(userName, srcGroups, dstGroups))
                }
            } else {
                println("File not found!")
            }

        }
    }

    void deactAllOlds() {
        Set<String> srcGroups = new HashSet<>()
        Set<String> dstGroups = new HashSet<>()
        srcGroups.addAll(p."srcGroups".split(","))
        dstGroups.addAll(p."dstGroups".split(","))
        if (p."dateRegex") {
            JiraUser.dateSearchRegex = p."dateRegex"
        }
        if (p."dateFormat") {
            JiraUser.dateFormatter = p."dateFormat"
        }
        authSudo(browser)
        openUserDir(browser)
        Set<JiraUser> allFiltered = new HashSet<>()
        if (browser.formUserDir.isDisplayed()) {
            int endPage = Math.ceil(browser.$("span", class: "results-count-total")[0].text().toInteger() / 100)
            int currentPage = 0
            String token
            if (isNextPageOnUserDir(browser)) {
                token = browser.$("a", class: "icon-next")[0].attr('href').split('&')[0]
            }
            while (currentPage < endPage) {
                Set filteredUsers = getFilteredUsers(p."daysAgo".toLong(), p."usernameFilter")
                allFiltered.addAll(filteredUsers)
                DataWriter.printUserList(filteredUsers)
                DataWriter.appendUsersCsvFile(filteredUsers, "deactivated")

                currentPage++
                openPageNumberOnUserDir(browser, token, currentPage * 100)
            }
            if (allFiltered.size() > 0) {
                allFiltered.each { JiraUser ju ->
                    moveUser(ju, srcGroups, dstGroups)
                }
            }

        }

    }

    Set<JiraUser> getFilteredUsers(long daysAgo = 180L, String filterName) {
        if (filterName != null) {
            return getUsersFromPage(browser).findAll { JiraUser ju ->
                ju.username ==~ filterName &&
                        (ju.isNeverLogin() || ju.isLastLoginMoreThan(daysAgo))
            }
        } else {
            return getUsersFromPage(browser).findAll { JiraUser ju ->
                (ju.isNeverLogin() || ju.isLastLoginMoreThan(daysAgo))
            }
        }
    }

    boolean moveUser(JiraUser user, srcG, dstG) {
        return this.moveUser(user.username, srcG, dstG)
    }

    boolean moveUser(String userName, srcG, dstG) {
        browser.go "/secure/admin/user/ViewUser.jspa?name=${userName}"
        authSudo(browser)
        boolean added = addToGroups(browser, dstG)
        browser.go "/secure/admin/user/ViewUser.jspa?name=${userName}"
        boolean removed = removeFromGroups(browser, srcG)
        return (added || removed)
    }

    void test() {
        Set srcG = ["jira-users"]
        JiraUser user = new JiraUser()
        user.username = "admin_c5287511"
        moveUser(user, srcG, srcG)
    }


}
