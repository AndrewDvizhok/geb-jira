package helpers

import java.time.LocalDate

class DataWriter {

    static File csv

    static void printUserList(Set<JiraUser> jiraUsers) {

        println("User list (${jiraUsers.size()}):")
        if (jiraUsers.size() > 0) {
            printf("|%20s|%20s|%20s|%20s|%20s|\n", "Full name", "Username", "Email", "Login details", "Directory")
            jiraUsers.each { JiraUser ju ->
                printf("|%20s|%20s|%20s|%20s|%20s|\n",
                        getShortString(ju.fullname),
                        getShortString(ju.username),
                        getShortString(ju.email),
                        getShortString(ju.getLoginStatus()),
                        getShortString(ju.directory)
                )
            }
        }

    }

    static String getShortString(String str) {
        return str.length() > 20 ? str.substring(0, 20) : str
    }

    static void appendUsersCsvFile(Set<JiraUser> jiraUsers, csvName="userList"){
        if(csv == null){
            csv = new File("$csvName${LocalDate.now()}.csv")
            csv.write("Full name,Username,Email,Login status,Directory\n")
        }
        jiraUsers.each { JiraUser ju ->
            csv.append("$ju.fullname, $ju.username, $ju.email, ${ju.getLoginStatus()}, $ju.directory\n")
        }
    }

}
