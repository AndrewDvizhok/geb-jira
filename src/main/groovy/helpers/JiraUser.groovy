package helpers

import java.time.LocalDate
import java.time.format.DateTimeFormatter

class JiraUser {
    static String dateFormatter = "dd.MM.yy"
    static String dateSearchRegex = "\\d{2}\\.\\d{2}\\.\\d{2}"
    String fullname
    String username
    String email
    String loginDetails
    String directory
    LocalDate lastLogin

    boolean isLastLoginMoreThan(Long daysAgo) {
        return lastLogin == null ? false : lastLogin.isBefore(LocalDate.now().minusDays(daysAgo))
    }

    boolean isNeverLogin() {
        return loginDetails.contains("Never")
    }

    void tryParseLastLoginDate(String logDet) {
        if(logDet != null){
            loginDetails = logDet
            if (loginDetails.contains("Last:") && (loginDetails =~ dateSearchRegex).size() > 0) {
                lastLogin = LocalDate.parse((loginDetails =~ dateSearchRegex)[0], DateTimeFormatter.ofPattern(dateFormatter))
            }
        }
    }

    String getLoginStatus() {
        if (isNeverLogin()) {
            return "Never"
        } else {
            return loginDetails.contains("CAPTCHA") ? "CAPTCHA from " + lastLogin.toString() : lastLogin.toString()
        }
    }

}
